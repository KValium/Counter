import React from 'react';
import { object, number } from 'prop-types';
import { withStyles } from '@material-ui/core';
import { AppBar, Toolbar, Typography } from '@material-ui/core';

// Material UI
const styles = {
  navBar: {
    flexGrow: 1
  }
};

const NavBar = ({ classes, counter }) => (
  <div className={classes.navBar}>
    <AppBar position="static" color="default">
      <Toolbar>
        <Typography variant="title" color="inherit">
          Counter : {counter}
        </Typography>
      </Toolbar>
    </AppBar>
  </div>
);

NavBar.defaultProps = {
  counter: 0
};

NavBar.propTypes = {
  classes: object.isRequired,
  counter: number
};

export default withStyles(styles)(NavBar);