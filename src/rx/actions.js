import { actionCreator } from './flux';
import * as Constants from './constants';

export const incrementCounter = actionCreator((range = 1) => ({
  type: Constants.INCREMENT_COUNTER,
  range
}));

export const decrementCounter = actionCreator((range = 1) => ({
  type: Constants.DECREMENT_COUNTER,
  range
}));
