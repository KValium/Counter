import * as Constants from './constants';

export default function reducer(state, action) {
  switch (action.type) {
    case Constants.INCREMENT_COUNTER:
      return {
        ...state,
        counter: state.counter + action.range
      };

    case Constants.DECREMENT_COUNTER:
    return {
      ...state,
      counter: state.counter - action.range
    };

    default:
      return state;
  }
}
